import axios from 'axios';

const httpClient = axios.create({
    baseURL: '/api',
});

export default {
    all() {
        return httpClient.get('users');
    },
    find(id) {
        return httpClient.get(`users/${id}`);
    },
    create(data) {
        return httpClient.post('users', data)
    },
    update(id, data) {
        return httpClient.put(`users/${id}`, data);
    },
    delete(id) {
        return httpClient.delete(`users/${id}`);
    },
}
