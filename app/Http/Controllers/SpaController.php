<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

final class SpaController extends Controller
{
    public function index()
    {
        return view('spa');
    }
}
