PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '

alias ll='ls -l'
alias l='ls -lA'
alias lla='ls -la'
alias pa='php artisan'
